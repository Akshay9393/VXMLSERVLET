package org.java.com;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloVoiceWorld
 */
@WebServlet("/HelloVoiceWorld")
public class HelloVoiceWorld extends HttpServlet
{
 public void doGet( HttpServletRequest req, HttpServletResponse res)
  throws IOException, ServletException
 {
  res.setContentType("text/xml");
  PrintWriter out = res.getWriter();
  out.println("<?xml version=\"1.0\"?> " );
  out.println("<vxml version=\"1.0\">");

  out.println("  <form>");
  out.println("   <block>");
  out.println("      Hello voice world!");
  out.println("      Spoken from a servlet.");
  out.println("   </block>");
  out.println("  </form>");
  out.println("</vxml>");
 } 
}
