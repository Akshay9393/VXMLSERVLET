package org.java.com;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloWorld
 */
@WebServlet("/HelloWorld")
public class HelloWorld extends HttpServlet
{
	 public void doGet( HttpServletRequest req, HttpServletResponse res)
	  throws IOException, ServletException
	 {
	  res.setContentType("text/html");
	  PrintWriter out = res.getWriter();
	  // Output HTML
	  out.println("<html><head><title>Hello world!");
	  out.println("</title></head><body>");
	  out.println("<h1>Hello world!</h1>");
	  out.println("<p>This is written from a servlet.</p>");
	  out.println("</body></html>");
	 } 
	}
